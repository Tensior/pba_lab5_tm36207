package io.swagger.switch_api.api;

import com.google.gson.Gson;
import io.swagger.switch_api.model.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-07T15:16:08.865+02:00")

@Controller
public class UsersApiController implements UsersApi {

    @Autowired
    UserRepository repository;

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "User object that has to be added" ,required=true )  @Valid @RequestBody CreateRequest body) {
        String accept = request.getHeader("Accept");
        User user = body.getUser();
        UUID id = UUID.randomUUID();
        user.setId(id);
        String json = new Gson().toJson(user);
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());



        if (accept != null && accept.contains("application/json")) {
            try {
                repository.save(user);
                return new ResponseEntity<UserResponse>(objectMapper.readValue(
                        "{  \"responseHeader\" : {" +
                                "    \"sendDate\" : \""+ time + "\"," +
                                "    \"requestId\" :  \"" + id +"\"  },  " +
                                "\"user\" : "+ json + " }", UserResponse.class), HttpStatus.CREATED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteUser(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        String accept = request.getHeader("Accept");
        UUID requestId = UUID.randomUUID();
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        User user = repository.getOne(id);
        if (user != null) {
            repository.delete(user);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<UserListResponse> getAllUsers() {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        String json = new Gson().toJson(repository.findAll());
        UUID requestId = UUID.randomUUID();


        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserListResponse>(objectMapper.readValue(
                        "{ \"usersList\" : "+json+",  " +
                                "\"responseHeader\" : {    \"sendDate\" : \""+time+"" + "\"," +
                                " \"requestId\" : \""+requestId+"\" }}", UserListResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserListResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserListResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        String json = new Gson().toJson(repository.getOne(id));
        UUID requestId = UUID.randomUUID();

        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserResponse>(objectMapper.readValue(
                        "{   \"responseHeader\" : {    \"sendDate\" : \""+time+"\"," +
                                "    \"requestId\" : \""+requestId.toString()+"\"  }," +
                                " \"user\" : "+json+"}", UserResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<UserResponse> updateUser(@ApiParam(value = "",required=true) @PathVariable("id") UUID id,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UpdateRequest body) {
        String accept = request.getHeader("Accept");
        TimeZone timeZone = TimeZone.getTimeZone("GMT+2:00");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(timeZone);
        String time = dateFormat.format(new Date());
        UUID requestId = UUID.randomUUID();
        User newUser = body.getUser();
        User user = repository.getOne(id);
        String json = new Gson().toJson(newUser);

        if (accept != null && accept.contains("application/json")) {
            if(user != null) {
                try {
                    repository.save(newUser);
                    return new ResponseEntity<UserResponse>(objectMapper.readValue(
                            "{  \"responseHeader\" : {    \"sendDate\" : \""+time+"\"," +
                                    "    \"requestId\" : \""+requestId+"\"  }," +
                                    "  \"user\" :"+json+"}", UserResponse.class), HttpStatus.OK);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }
        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

}
